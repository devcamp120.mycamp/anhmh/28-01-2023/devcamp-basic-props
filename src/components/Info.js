import { Component } from "react";

class Info extends Component {
    render() {
        let {firstName, lastName, favNumber} = this.props;
        return (
            <p>My name is {lastName} {firstName} and my favourite number is {favNumber}. </p>
        )
    }
}

export default Info;